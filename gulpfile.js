var elixir = require('laravel-elixir'),
    gulp = require('gulp');

config.assetsPath = 'assets/app';
config.publicPath = 'assets';
config.css.autoprefix.enabled = true;

elixir(function (mix) {
    mix.sass('main.scss')
        .scripts([
            'vendor/**/*.js'
        ], 'assets/js/vendor.js')
        .scripts([
            'main.js',
            'modules/**/*.js'
        ], 'assets/js/main.js')
        .scripts([
            'admin/**/*.js'
        ], 'assets/js/admin.js');


});